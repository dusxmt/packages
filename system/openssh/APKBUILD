# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=openssh
pkgver=8.1_p1
_myver=${pkgver%_*}${pkgver#*_}
pkgrel=0
pkgdesc="Port of OpenBSD's free SSH release"
url="https://www.openssh.com/portable.html"
arch="all"
license="BSD-1-Clause AND BSD-2-Clause AND BSD-3-Clause"
options="suid !check"
depends="openssh-client openssh-sftp-server openssh-server"
makedepends_build="linux-pam-dev gettext-tiny"
makedepends_host="openssl-dev zlib-dev linux-headers linux-pam-dev
	gettext-tiny-dev utmps-dev"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-doc
	$pkgname-keygen
	$pkgname-client
	$pkgname-keysign
	$pkgname-sftp-server:sftp
	$pkgname-server
	$pkgname-openrc
	"

source="https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$pkgname-$_myver.tar.gz
	disable-forwarding-by-default.patch
	fix-utmpx.patch
	sftp-interactive.patch
	time64-seccomp.patch

	sshd.initd
	sshd.confd
	"
# secfixes:
#   7.9_p1-r2:
#     - CVE-2018-20685
#   7.9_p1:
#     - CVE-2018-15473
#   7.7_p1:
#     - CVE-2017-15906
#   7.4_p1:
#     - CVE-2016-10009
#     - CVE-2016-10010
#     - CVE-2016-10011
#     - CVE-2016-10012

# HPN patches are from: http://hpnssh.sourceforge.net/

builddir="$srcdir"/$pkgname-$_myver

build() {
	export LD="$CC"
	LIBS="-lutmps -lskarnet" ./configure --build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc/ssh \
		--libexecdir=/usr/lib/ssh \
		--mandir=/usr/share/man \
		--with-pid-dir=/run \
		--with-mantype=man \
		--with-ldflags="${LDFLAGS}" \
		--enable-lastlog \
		--disable-strip \
		--enable-wtmp \
		--with-privsep-path=/var/empty \
		--with-xauth=/usr/bin/xauth \
		--with-privsep-user=sshd \
		--with-md5-passwords \
		--with-ssl-engine \
		--with-pam
	make
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p "$pkgdir"/var/empty
	install -D -m755 "$srcdir"/sshd.initd \
		"$pkgdir"/etc/init.d/sshd
	install -D -m644 "$srcdir"/sshd.confd \
		"$pkgdir"/etc/conf.d/sshd
	install -Dm644 "$builddir"/contrib/ssh-copy-id.1 \
		"$pkgdir"/usr/share/man/man1/ssh-copy-id.1
}

keygen() {
	pkgdesc="Helper program for generating SSH keys"
	depends=""
	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ssh-keygen \
		"$subpkgdir"/usr/bin/
}

client() {
	pkgdesc="OpenBSD's SSH client"
	depends="openssh-keygen"
	install -d "$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/lib/ssh \
		"$subpkgdir"/etc/ssh \
		"$subpkgdir"/var/empty

	mv "$pkgdir"/usr/bin/* \
		"$subpkgdir"/usr/bin/
	mv "$pkgdir"/etc/ssh/ssh_config \
		"$pkgdir"/etc/ssh/moduli \
		"$subpkgdir"/etc/ssh/
	install -Dm755 "$builddir"/contrib/findssl.sh \
		"$subpkgdir"/usr/bin/findssl.sh
	install -Dm755 "$builddir"/contrib/ssh-copy-id \
		"$subpkgdir"/usr/bin/ssh-copy-id
	install -Dm755	"$builddir"/ssh-pkcs11-helper \
		"$subpkgdir"/usr/bin/ssh-pkcs11-helper
}

keysign() {
	pkgdesc="Helper program for SSH host-based authentication"
	depends="openssh-client"
	install -d "$subpkgdir"/usr/lib/ssh
	mv "$pkgdir"/usr/lib/ssh/ssh-keysign \
		"$subpkgdir"/usr/lib/ssh/
}

sftp() {
	pkgdesc="SFTP server module for OpenSSH"
	depends=""
	install -d "$subpkgdir"/usr/lib/ssh
	mv "$pkgdir"/usr/lib/ssh/sftp-server \
		"$subpkgdir"/usr/lib/ssh/
}

server() {
	pkgdesc="OpenSSH server"
	depends="openssh-client openssh-keygen"
	replaces="openssh-server-common"

	install -d "$subpkgdir"/usr/sbin
	install -d "$subpkgdir"/etc/ssh
	mv "$pkgdir"/usr/sbin/sshd "$subpkgdir"/usr/sbin/
	mv "$pkgdir"/etc/ssh/sshd_config "$subpkgdir"/etc/ssh/
}

openrc() {
	default_openrc
	depends="openssh-server"
	install_if="openssh-server=$pkgver-r$pkgrel openrc"
}

sha512sums="b987ea4ffd4ab0c94110723860273b06ed8ffb4d21cbd99ca144a4722dc55f4bf86f6253d500386b6bee7af50f066e2aa2dd095d50746509a10e11221d39d925  openssh-8.1p1.tar.gz
f3d5960572ddf49635d4edbdff45835df1b538a81840db169c36b39862e6fa8b0393ca90626000b758f59567ff6810b2537304098652483b3b31fb438a061de6  disable-forwarding-by-default.patch
9033520d18ccfea87628c78008591ae8a143999868254eabc926ca0665611c9f09c221265b1b6f552b82eca58558244a020d615b55249a02f96e298c1f7ff520  fix-utmpx.patch
34c0673f550e7afcd47eda4fe1da48fb42e5344c95ba8064c9c3c137fda9c43635b0f7b8145d0300f59c79f75a396ebd467afb54cdaa42aa251d624d0752dc84  sftp-interactive.patch
ad5b209f7f3fff69c10bae34da143e071e107a2141eee94f393532d6bb04a36bfe6d9b5d2c08b713f67118503c38d11b4aad689df1df7c8a918d52db8326821d  time64-seccomp.patch
394a420a36880bb0dd37dfd8727cea91fd9de6534050169e21212a46513ef3aaafe2752c338699b3d4ccd14871b26cf01a152df8060cd37f86ce0665fd53c63f  sshd.initd
ce0abddbd2004891f88efd8522c4b37a4989290269fab339c0fa9aacc051f7fd3b20813e192e92e0e64315750041cb74012d4321260f4865ff69d7a935b259d4  sshd.confd"
