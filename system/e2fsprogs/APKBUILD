# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=e2fsprogs
pkgver=1.45.6
pkgrel=0
pkgdesc="Ext2/3/4 filesystem utilities"
url="http://e2fsprogs.sourceforge.net"
arch="all"
license="GPL-2.0-only AND LGPL-2.0-only AND MIT"
depends=""
depends_dev="util-linux-dev"
makedepends="$depends_dev linux-headers"
subpackages="$pkgname-lang $pkgname-dev $pkgname-doc libcom_err $pkgname-libs"
source="https://www.kernel.org/pub/linux/kernel/people/tytso/$pkgname/v$pkgver/$pkgname-$pkgver.tar.xz
	header-fix.patch
	time64.patch
	"

# secfixes:
#   1.45.3-r1:
#     - CVE-2019-5094

build () {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--mandir=/usr/share/man \
		--enable-elf-shlibs \
		--enable-symlink-install \
		--disable-fsck \
		--disable-uuidd \
		--disable-libuuid \
		--disable-libblkid \
		--disable-tls
	make
}

check() {
	make check
}

package() {
	make -j1 LDCONFIG=: DESTDIR="$pkgdir" install install-libs
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/bin/compile_et "$pkgdir"/usr/bin/mk_cmds \
		"$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share/et "$pkgdir"/usr/share/ss \
		"$subpkgdir"/usr/share
}

libcom_err() {
	pkgdesc="Common error description library"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libcom_err* "$subpkgdir"/lib/
}

sha512sums="f3abfb6fe7ef632bb81152e2127d601cadd3fa93162178576a1d5ed82c2286627184b207b85a5b2a1793db0addf0885dfc3b9523bb340443224caf9c6d613b84  e2fsprogs-1.45.6.tar.xz
34ca45c64a132bb4b507cd4ffb763c6d1b7979eccfed20f63417e514871b47639d32f2a3ecff090713c21a0f02ac503d5093960c80401d64081c592d01af279d  header-fix.patch
5f8ff2f096da4b445edff72dfa03f27dd8bf5b6733e724205f5661e32fd0bae92849a1aa53e20c6b60c026dc5ed68567b7866fade1ecd0187718509f03fe9145  time64.patch"
