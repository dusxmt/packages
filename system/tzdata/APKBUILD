# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tzdata
pkgver=2020a
_tzcodever=2020a
_ptzver=0.5
pkgrel=0
pkgdesc="Time zone data"
url="https://www.iana.org/time-zones"
arch="all"
options="!check"  # Until we have sgml-common.
license="Public-Domain"
depends=""
checkdepends="sgml-common sp"
makedepends=""
subpackages="$pkgname-doc"
source="https://www.iana.org/time-zones/repository/releases/tzcode$_tzcodever.tar.gz
	https://www.iana.org/time-zones/repository/releases/tzdata$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/posixtz/posixtz-$_ptzver.tar.xz
	0001-posixtz-fix-up-lseek.patch"

builddir="$srcdir"
_timezones="africa antarctica asia australasia europe northamerica \
	southamerica pacificnew etcetera backward systemv factory"

build() {
	make cc="${CC:-gcc}" CFLAGS="$CFLAGS -DHAVE_STDINT_H=1"
		TZDIR="/usr/share/zoneinfo"

	make -C posixtz-$_ptzver posixtz
}

check() {
	make -j1 check
}

package() {
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo ${_timezones}
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/right -L leapseconds ${_timezones}
	#./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/posix ${_timezones}

	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo -p America/New_York
	install -m444 -t "$pkgdir"/usr/share/zoneinfo iso3166.tab zone1970.tab zone.tab

	mkdir -p "$pkgdir"/usr/sbin
	install -m755 zic zdump "$pkgdir"/usr/sbin

	mkdir -p "$pkgdir"/usr/share/man/man8
	install -m644 zic.8 zdump.8 "$pkgdir"/usr/share/man/man8

	rm -f "$pkgdir"/usr/share/zoneinfo/localtime
	install -Dm755 "$srcdir"/posixtz-$_ptzver/posixtz \
		"$pkgdir"/usr/bin/posixtz
}

sha512sums="5235d6590e844491df32375857f5f36ba0046f2d21a078375188dff79b125cf9b69136a0a431624de1ecfa9d53b62a232f0e6ee0b734294cec62b670e6265cb2  tzcode2020a.tar.gz
2a2fc2e3ad8a6e4c574242296c847ad582c2c1d86add9c556e65c812d19b9528522e3c4dddb5239017091825d2acc5a2ccaf21dc41b900b6c300ef4264cc5a9d  tzdata2020a.tar.gz
68dbaab9f4aef166ac2f2d40b49366527b840bebe17a47599fe38345835e4adb8a767910745ece9c384b57af815a871243c3e261a29f41d71f8054df3061b3fd  posixtz-0.5.tar.xz
f54ce213d74c5a8387e1a7c56299bc6eee65a035772288222128abc249a112067b8791b88b45c342b2d4d8d12e9e4f1f2f5c92c5de67f8b6413b1ebf1d7de467  0001-posixtz-fix-up-lseek.patch"
