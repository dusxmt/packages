# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Adélie Perl Team <adelie-perl@lists.adelielinux.org>
pkgname=perl-unicode-linebreak
_pkgreal=Unicode-LineBreak
_author=NEZUMI
_au=${_author%%"${_author#??}"}
_a=${_author%%"${_author#?}"}
pkgver=2019.001
_univer=8.0.0 # update this and unicode.org URLs if sombok is ever updated
pkgrel=0
pkgdesc="Perl implementation of the UAX#14 Unicode line-breaking algorithm"
url="https://metacpan.org/release/Unicode-LineBreak"
arch="all"
license="Artistic-1.0-Perl AND GPL-2.0+"
depends="perl-mime-charset"
makedepends="perl-dev"
subpackages="$pkgname-doc"
source="https://cpan.metacpan.org/authors/id/$_a/$_au/$_author/$_pkgreal-$pkgver.tar.gz
	https://www.unicode.org/Public/$_univer/ucd/auxiliary/GraphemeBreakTest.txt
	https://www.unicode.org/Public/$_univer/ucd/auxiliary/LineBreakTest.txt"
builddir="$srcdir/$_pkgreal-$pkgver"

build() {
	cp "$srcdir"/*Test.txt "$builddir"/test-data
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete
}

sha512sums="2d285f928db2ed32aefa741629ce9f9bee82e69c247eaa2c9145fdb4e9bae4441bf822e108cbffc3955369539474c3299f0a8f69026a5f9686aa8458a1d87f27  Unicode-LineBreak-2019.001.tar.gz
cc5a4fa7d53c127f749d0c45582f55f391ffdcfa2f2c91c41cb0a4f71e566f1657d4653a6a251c6ae4f769420701df6e0cf184e55413c602cbc5191f9dc99201  GraphemeBreakTest.txt
d2ffb02fb4d402e6a90937743721692c008b78045e295bd77bcdfcabc99fb1fdbfb0b4679b410bce4487eba115a7ef2e6d6bb03b0a47a7220fa4308eafbac74e  LineBreakTest.txt"
