# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cmake
pkgver=3.16.4
pkgrel=0
pkgdesc="Cross-platform build system"
url="https://cmake.org"
arch="all"
license="CMake"
depends=""
checkdepends="musl-utils file"
makedepends="ncurses-dev curl-dev expat-dev zlib-dev bzip2-dev libarchive-dev
	libuv-dev xz-dev rhash-dev"
subpackages="$pkgname-doc"

case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac

source="https://cmake.org/files/$_v/cmake-${pkgver}.tar.gz"

_parallel_opt() {
	local i n
	for i in $MAKEOPTS; do
		case "$i" in
			-j*) n=${i#-j};;
		esac;
	done
	[ -n "$n" ] && echo "--parallel $n"
}

build() {
	# jsoncpp needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version of jsoncpp
	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--system-libs \
		--no-system-jsoncpp \
		$(_parallel_opt)
	make
}

check() {
	# skip CTestTestUpload: tries to upload something during check...
	CTEST_OUTPUT_ON_FAILURE=TRUE bin/ctest -E CTestTestUpload
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4aa36608bac3b9e29ea765003cdc684733c025d7b18fbae057f3be1f726a159d4b0231b8b1be269206aa7dbd7177fe0110d5bac8d72fb0f6ad392bb284b1fce3  cmake-3.16.4.tar.gz"
