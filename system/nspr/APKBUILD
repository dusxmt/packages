# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=nspr
pkgver=4.27
pkgrel=0
pkgdesc="Netscape Portable Runtime"
url="https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSPR"
arch="all"
options="!check"  # No test suite.
license="MPL-1.1 AND GPL-2.0-only AND LGPL-2.1-only"
depends=""
# -dev package does not ship any symlinks so dependency cannot be autodetected
depends_dev="nspr"
makedepends="autoconf automake"
subpackages="$pkgname-dev"
source="https://ftp.mozilla.org/pub/mozilla.org/nspr/releases/v$pkgver/src/nspr-$pkgver.tar.gz
	stacksize.patch
	"

prepare() {
	mkdir build inst
	default_prepare
}

build() {
	case "$CARCH" in
		*64* | s390x) _conf="--enable-64bit";;
	esac
	cd "$builddir"/build
	# ./nspr/pr/include/md/_linux.h tests only __GLIBC__ version
	# to detect c-library features, list musl features here for now.
	CFLAGS="$CFLAGS -D_PR_POLL_AVAILABLE -D_PR_HAVE_OFF64_T -D_PR_INET6 -D_PR_HAVE_INET_NTOP -D_PR_HAVE_GETHOSTBYNAME2 -D_PR_HAVE_GETADDRINFO -D_PR_INET6_PROBE" \
	../nspr/configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-debug \
		--enable-optimize \
		--enable-ipv6 \
		$_conf
	make CC="${CC:-gcc}" CXX="${CXX:-g++}"
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install

	cd "$pkgdir"/usr/lib
	rm -f *.a

	cd "$builddir"/build/config
	install -Dm755 nspr-config "$pkgdir"/usr/bin/nspr-config
	install -Dm644 nspr.pc "$pkgdir"/usr/lib/pkgconfig/nspr.pc
	rm -rf "$pkgdir"/usr/bin/prerr.properties \
		"$pkgdir"/usr/bin/compile-et.pl \
		"$pkgdir"/usr/share/aclocal/nspr.m4 \
		"$pkgdir"/usr/include/nspr/md
}

sha512sums="2be539e6fd5ed8987874147a2bacc3f0b7177cdfddbb1a4f9e5f93575de871584b05fb56ca4e1ba5f7e53074cd4069310658f1559de0f38def208b087b6e6dca  nspr-4.27.tar.gz
1f694fc151f6578080449e3aa999c520486bbe117b8237150966ec43092db4156e81412ac889045e0c0c3bf65d459af5bdc1cf19c9fa3dab120405a60732f15a  stacksize.patch"
