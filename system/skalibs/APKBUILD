# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=skalibs
pkgver=2.10.0.0
pkgrel=0
pkgdesc="A set of general-purpose C programming libraries for skarnet.org software"
url="https://skarnet.org/software/skalibs/"
arch="all"
options="!check"  # No test suite.
license="ISC"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--libdir=/usr/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}


libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends=""
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
	pkgdesc="$pkgdesc (development files)"
	depends=""
	install_if="dev $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"
	mv "$pkgdir/usr" "$subpkgdir/"
}


libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)" 
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
	pkgdesc="$pkgdesc (documentation)" 
	depends=""
	install_if="docs $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}


sha512sums="e8b36388b24ee8066e48d39650a27030374f263f0ee014eaa79f9d71f2b5647dd1a052cc8ffe0d8457fed8fd1b1fc74980c03dd1182a9d0f1efbe044bff99269  skalibs-2.10.0.0.tar.gz"
