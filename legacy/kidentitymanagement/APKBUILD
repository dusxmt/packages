# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kidentitymanagement
pkgver=18.04.3
pkgrel=0
pkgdesc="Library for managing user identities"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Test requires X11 desktop running.
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kcompletion-dev ktextwidgets-dev
	kxmlgui-dev kio-dev kconfig-dev kcodecs-dev kiconthemes-dev
	kpimtextedit-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4e6ad309d18e2bb88a86a1b236bb29782aede0ca18cc5e4ac76926e17030ec6367d80fda50dab4c29fcf5a4475560e027edbb7cc9cdedf2c3d6f9dba7ff6697b  kidentitymanagement-18.04.3.tar.xz"
