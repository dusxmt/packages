# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmediaplayer
pkgver=5.74.0
pkgrel=0
pkgdesc="Media player framework for KDE 5"
url="https://www.kde.org/"
arch="all"
license="X11 AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kparts-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kmediaplayer-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# viewtest requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E viewtest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e7d0ba3d2720b9d1dc99cf4f4b11ab2f7c949181d6f1a8bd83ab476d248a67c4f6440e7786b26424347c08b3b8e03fbbf3de1184ce09a7071de501be452eec4c  kmediaplayer-5.74.0.tar.xz"
