# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=qtermwidget
pkgver=0.15.0
_lxqt_build=0.7.0
pkgrel=0
pkgdesc="Qt-based terminal widget, used in QTerminal"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
depends_dev="libutempter-dev"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	lxqt-build-tools>=$_lxqt_build $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/qtermwidget/releases/download/$pkgver/qtermwidget-$pkgver.tar.xz
	fix-linking.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DQTERMWIDGET_USE_UTEMPTER=True \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
        CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="95f44c77d0b678b1a204b559a7adb2fb68b2e82b2357d89239e9fc1c6f445346d84ec7f395d58f17a2ef7b42e58c02590f637683193eae4f7efbd001b61857c7  qtermwidget-0.15.0.tar.xz
f2d43f12479a3a3a01be9ebee9ce349b0f4e60b07dfc79e8666ed913d5682987f81b08a397ee8ca628c36f5770ddfe63f5abafa94dff6c1c1e090adc1637af35  fix-linking.patch"
