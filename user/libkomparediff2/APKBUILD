# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkomparediff2
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE diff library"
url="https://kde.org/applications/development/org.kde.kompare"
arch="all"
license="GPL-2.0+ AND GPL-2.0-only AND LGPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev ki18n-dev
	kio-dev kitemviews-dev kjobwidgets-dev kservice-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev solid-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkomparediff2-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="043982200ea9c51f114a80c80865f04fc028b5462c66185f76cf3cc7eac80f54733adc6ff13ca0311525507e13f0fb3eb647f32092542e20a412ae4f859bfe76  libkomparediff2-20.08.1.tar.xz"
