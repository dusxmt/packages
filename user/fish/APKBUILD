# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=fish
pkgver=3.1.2
pkgrel=0
pkgdesc="Modern interactive commandline shell"
url="http://www.fishshell.com"
arch="all"
license="BSD-3-Clause AND BSD-2-Clause AND GPL-2.0+ AND GPL-2.0-only AND ISC"
depends="bc groff"
depends_dev="$pkgname-tools"
checkdepends="expect"
makedepends="cmake doxygen ncurses-dev pcre2-dev"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-tools::noarch"
source="https://github.com/fish-shell/fish-shell/releases/download/$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCURSES_NEED_NCURSES=ON \
		-Bbuild .
	make -C build
}

check() {
	make -C build test
}

package() {
	make -C build install DESTDIR="$pkgdir"
}

dev() {
	default_dev

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/pkgconfig "$subpkgdir"/usr/share
}

doc() {
	default_doc

	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/man "$subpkgdir"/usr/share/$pkgname
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	depends="$pkgname python3"

	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/tools "$subpkgdir"/usr/share/$pkgname
}

sha512sums="b6ae2c928774a2eaccf35312d3a9446bfa3e1335182c8f2b2d6198161d0916904f4964fb20ed13a5bf850c1c819e003905d13db3bc8b1faa5b401a60b47dc563  fish-3.1.2.tar.gz"
