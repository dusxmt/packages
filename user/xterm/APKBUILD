# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=xterm
pkgver=360
pkgrel=0
pkgdesc="An X-based terminal emulator"
url="https://invisible-island.net/xterm/"
arch="all"
options="!check"  # No test suite.
license="X11"
depends=""
makedepends="libx11-dev libsm-dev libice-dev libxt-dev utmps-dev libxaw-dev
	libxext-dev libxrender-dev libxft-dev freetype-dev ncurses-dev
	pcre-dev"
subpackages="$pkgname-doc"
source="https://invisible-mirror.net/archives/xterm/xterm-$pkgver.tgz
	posix-ptmx.patch
	"

build() {
	LIBS="-ltinfow" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-pcre \
		--enable-sixel-graphics
	# This is NOT A TYPO!
	#
	# XTerm does not use ld(1) as a linker.  It uses a shell script
	# called 'plink.sh' which tries to Be Smart, but is actually
	# Quite Dumb.
	#
	# It determines that the utmp symbols are in musl, and decides
	# -lutmps really isn't necessary.  However!  There is some solace.
	#
	# -k is like -l, but is forced, even if it isn't "really needed".
	# So we use -k for utmps.
	make EXTRA_LOADFLAGS="-kutmps -lskarnet"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ffca51e21584e4b84b271a555bead45df1759a9e353d776fd7d8736fb001b71cfa14dc85336c01c304b4fc98297523b1a943792da73b453e1810b890671bb607  xterm-360.tgz
6811cd67ffe21dc23c9ad6a439291bb0b3c3d347f4d5e2cd65de9f790fde2bf7323fe9f1b309d95ac98a3ffaae5989bc73a2c5fe08aa9f6a2c6cbc3b9adcf8d9  posix-ptmx.patch"
