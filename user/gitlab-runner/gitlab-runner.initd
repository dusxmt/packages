#!/sbin/openrc-run

: "${CONFIG_FILE:=/etc/gitlab-runner/config.toml}"
: "${command_args:=--working-directory /var/lib/gitlab-runner}"
: "${logfile:=/var/log/gitlab-runner.log}"
: "${timeout:=300}"
: "${retry:=60}"
: "${command_user:=gitlab-runner:gitlab-runner}"
export CONFIG_FILE

command="/usr/bin/gitlab-runner"
command_args="run --service ${RC_SVCNAME} ${command_args}"
command_background="true"
start_stop_daemon_args="--stdout ${logfile} --stderr ${logfile}"
pidfile="/run/${RC_SVCNAME}.pid"

extra_started_commands="reload gracefulstop"
description_reload="Reload configuration"
description_gracefulstop="Finish current jobs, then stop"

start_pre() {
	checkpath -f -m 640 -o "${command_user}" "${logfile}"
}

reload() {
	ebegin "Reloading ${RC_SVCNAME} configuration"
	start-stop-daemon --quiet --signal HUP --pidfile "${pidfile}"
	eend $?
}

gracefulstop() {
	ebegin "Gracefully stopping ${RC_SVCNAME}"
	mark_service_stopping "${RC_SERVICE}"
	start-stop-daemon --quiet --progress --stop --pidfile "${pidfile}" \
		--signal QUIT --retry "${timeout}"
	if eend $?; then
		mark_service_stopped "${RC_SVCNAME}"
	else
		eerror "${RC_SVCNAME} timed out, forcefully stopping"
		if stop; then
			mark_service_stopped "${RC_SVCNAME}"
		else
			return 1
		fi
	fi
}
