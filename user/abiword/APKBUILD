# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=abiword
pkgver=3.0.4
pkgrel=0
pkgdesc="A fully-featured word processor"
url="https://www.abisource.com"
arch="all"
options="!check"  # Test suite requires valgrind, etc
license="GPL-2.0+"
depends=""
makedepends="bzip2-dev enchant-dev fribidi-dev goffice-dev gtk+3.0-dev
	libgsf-dev libjpeg-turbo-dev librsvg-dev libxslt-dev pcre-dev
	popt-dev wv-dev"
# openxml plugin
makedepends="$makedepends boost-dev"
# collab plugin
makedepends="$makedepends gnutls-dev libsoup-dev dbus-glib-dev"
subpackages="$pkgname-dev $pkgname-doc"
_plugins="applix babelfish bmp clarisworks collab docbook eml epub \
	freetranslation garble gdict gimp google hancom hrtext iscii kword \
	latex loadbindings mht mif mswrite openwriter openxml opml paint \
	passepartout pdb pdf presentation s5 sdw t602 urldict wikipedia wml \
	xslfo"
source="https://www.abisource.com/downloads/$pkgname/$pkgver/source/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-shared \
		--disable-static \
		--enable-plugins="$_plugins"
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="121708e010cef6722cab5622dc762f2f92c46e43b65b90b881b4f25ff32c1696fa2d281a3a6f2c04d5b9231bbbd0040281e9d4d7e594ec0ace6caae9c9021ec4  abiword-3.0.4.tar.gz"
