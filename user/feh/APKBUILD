# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=feh
pkgver=3.4.1
pkgrel=0
pkgdesc="feh is a fast, lightweight image viewer which uses imlib2"
url="https://feh.finalrewind.org/"
arch="all"
license="MIT-feh"
depends=""
checkdepends="perl-test-command"
makedepends="curl-dev imlib2-dev libexif-dev libjpeg-turbo-dev libpng-dev
	libxinerama-dev libxt-dev"
subpackages="$pkgname-doc"
source="https://feh.finalrewind.org/$pkgname-$pkgver.tar.bz2
	getopt.patch
	"

prepare() {
	default_prepare
	rm src/getopt.c src/getopt1.c src/getopt.h
}

build() {
	make PREFIX=/usr curl=1 exif=1 xinerama=1
}

check() {
	make PREFIX=/usr test
}

package() {
	make PREFIX=/usr DESTDIR="$pkgdir" \
		bin_dir="$pkgdir"/usr/bin \
		man_dir="$pkgdir"/usr/share/man \
		doc_dir="$pkgdir"/usr/share/doc/$pkgname \
		example_dir="$pkgdir"/usr/share/doc/$pkgname/examples \
		install
}

sha512sums="d674d3134819cf6731aebf8af87a2f890d54dd7f8c64071e8816a032aeeb95c31f56752bb3477be0dfb653f813872b32dc2daf4e0514fed03e3f6bba36896c66  feh-3.4.1.tar.bz2
cf02fbfcb1fd4f4e5cf899f7f94fd42521dbe8f1b805268eae5ae6222b85884c31abc3346a95ed46dfece7b89b445018e159ff8a42b799b3db18275e29eba1e3  getopt.patch"
