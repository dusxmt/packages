# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=nano
pkgver=4.9.3
pkgrel=0
pkgdesc="Enhanced clone of the Pico text editor"
url="https://www.nano-editor.org"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="file-dev ncurses-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://www.nano-editor.org/dist/v${pkgver%.*.*}/$pkgname-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-wordbounds
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 doc/sample.nanorc \
		"$pkgdir"/etc/nanorc

	# Proper syntax highlighting for APKBUILDs
	sed -i "/syntax/s/\"$/\" \"APKBUILD&/" \
		"$pkgdir"/usr/share/nano/sh.nanorc

	rm -rf "$pkgdir"/usr/lib/charset.alias
}

sha512sums="eefb88d7141f0c542a2d9b34fc8a079a92e512df0e9dbadcb3d780008ae19faf10296213ecdbf370fa8b81f5f3e81e2d419f5f76e62d669e61997d199a96ba18  nano-4.9.3.tar.xz"
