# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-mime
pkgver=20.08.1
pkgrel=0
pkgdesc="Libraries to implement basic MIME message handling"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev akonadi-dev kio-dev kmime-dev"
makedepends="$depends_dev cmake extra-cmake-modules libxslt-dev kdbusaddons-dev
	kconfig-dev kitemmodels-dev kxmlgui-dev shared-mime-info"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-mime-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# The mailserializerplugintest test requires kdepim-runtime to be
	# present on the system.  Otherwise, Akonadi does not know how to
	# convert a QByteArray of message/rfc822 to KMime::Message.  This
	# is a circular dependency, because kdepim-runtime requires amime
	# so we must skip this test.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'mailserializerplugintest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7bc468d93ba40ded4d3cb3fac23a2e1a06d76e3182c6b938fb21e09e6169ac96b8f5519becdc6d546ac269245f67b286afc37c6026cb05bc91e2e74b4f64a134  akonadi-mime-20.08.1.tar.xz
3feb62fbb888cb15b695a2cc86ff3d7cb8f63257f1185e1d87bc6b62cd0eb83e8b3c64eda37826eb5f97803a64f28c35ee4db40195be5c213569448535dde7b5  lts.patch"
