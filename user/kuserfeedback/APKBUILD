# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kuserfeedback
pkgver=1.0.0
pkgrel=0
pkgdesc="User feedback agent for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11.
license="MIT"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev qt5-qttools-dev bison flex"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/kuserfeedback/kuserfeedback-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	# Requires OpenGL accelerated desktop.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E openglinfosourcetest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d45c45b3f7b50f7ff3e5e5ae7e6a4cd68c0aa444553d0c44a969ae17e51ef5114ab6401c8ce65744ec3d635cb2edc7a127187547f01f4954faa55087c702dea7  kuserfeedback-1.0.0.tar.xz"
