# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=node
pkgver=10.21.0
pkgrel=0
pkgdesc="JavaScript runtime"
url="https://nodejs.org/"
arch="all"
options="net"  # Required in check()
license="MIT AND ICU AND BSD-3-Clause AND BSD-2-Clause AND ISC AND Public-Domain AND Zlib AND Artistic-2.0 AND Apache-2.0 AND CC0-1.0"
depends=""
makedepends="c-ares-dev http-parser-dev icu-dev libexecinfo-dev libuv-dev
	nghttp2-dev>=1.41 openssl-dev python3 zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://nodejs.org/download/release/v$pkgver/node-v$pkgver.tar.xz
	https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tar.xz
	libatomic.patch
	ppc32.patch
	ppc64.patch
	stack-silliness.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

# secfixes:
#   10.16.3-r0:
#     - CVE-2019-9511
#     - CVE-2019-9512
#     - CVE-2019-9513
#     - CVE-2019-9514
#     - CVE-2019-9515
#     - CVE-2019-9516
#     - CVE-2019-9517
#     - CVE-2019-9518
#   10.21.0-r0:
#     - CVE-2020-7598
#     - CVE-2020-8174

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	# TODO: when bumping to 12.x, python3 should be usable
	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-2.7.15"
	[ -d ../python ] && rm -r ../python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	make -j $JOBS install
}

build() {
	export PATH="$srcdir/python/bin:$PATH"
	python ./configure.py \
		--prefix=/usr \
		--with-intl=system-icu \
		--shared-cares \
		--shared-http-parser \
		--shared-libuv \
		--shared-nghttp2 \
		--shared-openssl \
		--openssl-use-def-ca-store \
		--shared-zlib
	# keep DESTDIR set, to avoid a full rebuild in package()
	make DESTDIR="$pkgdir"
}

check() {
	case "$CARCH" in
	pmmx)
		# https://bts.adelielinux.org/show_bug.cgi?id=306
		_skip="parallel/test-http-invalid-te,parallel/test-worker-stdio"
		;;
	esac

	export PATH="$srcdir/python/bin:$PATH"
	make DESTDIR="$pkgdir" test-only \
		${_skip:+CI_SKIP_TESTS="$_skip"}
}

package() {
	export PATH="$srcdir/python/bin:$PATH"
	make DESTDIR="$pkgdir" install
}

sha512sums="613d3c1bca79ea5f127dc6793de2b5cfdfa056c01ec092e3b7ee79205894b21ca5ec4a367265122641dd1d360c675cfb36a4f7892894194ddd18abd1b2206544  node-v10.21.0.tar.xz
27ea43eb45fc68f3d2469d5f07636e10801dee11635a430ec8ec922ed790bb426b072da94df885e4dfa1ea8b7a24f2f56dd92f9b0f51e162330f161216bd6de6  Python-2.7.15.tar.xz
8f64922d586bce9d82c83042a989739cc55ecc5e015778cdfbda21c257aa50527ddb18740985bcb2068e4a749b71eb8a135d9a8152b374d361589df7f33c9b60  libatomic.patch
6d37794c7c78ef92ebb845852af780e22dc8c14653b63a8609c21ab6860877b9dffc5cf856a8516b7978ec704f312c0627075c6440ace55d039f95bdc4c85add  ppc32.patch
583326353de5b0ac14a6c42321f6b031bd943a80550624794e15bd7526470f67bfa14a66558db3c94b4ee2db3053d2e4efed2117f4e7b6dca3c59c171048c094  ppc64.patch
3ea09e36ed0cc31e0475ebc9c92b7609b70e9c1637c5db6c92cf1d6363fb8c6f884ffa20dd81054ca390b721695185327d80c9eeff0688a959e9d46947602471  stack-silliness.patch"
