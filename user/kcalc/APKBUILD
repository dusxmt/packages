# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalc
pkgver=20.08.1
pkgrel=0
pkgdesc="Calculator with many mathematical, scientific, and logic functions"
url="https://utils.kde.org/projects/kcalc/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev ki18n-dev
	kconfigwidgets-dev kdoctools-dev kguiaddons-dev kinit-dev kxmlgui-dev
	knotifications-dev gmp-dev mpfr-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalc-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="13e9fc58d3a60934d5d648da113ff40701a4a81d0618f575c9cc7f6af4b7c9b283e9c699b448e83d462a63774d1e507df4d7a442360782425302ff9b0081e754  kcalc-20.08.1.tar.xz"
