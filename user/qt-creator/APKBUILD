# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt-creator
pkgver=4.12.4
pkgrel=0
pkgdesc="Cross-platform multi-language programming IDE"
url="https://doc.qt.io/qtcreator/index.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.0 WITH Qt-LGPL-exception-1.1"
depends="qt5-qtquickcontrols"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtserialport-dev
	qt5-qtscript-dev qt5-qttools-dev clang-dev llvm8-dev python3
	libexecinfo-dev"
subpackages="$pkgname-dev"
source="https://download.qt.io/official_releases/qtcreator/${pkgver%.*}/$pkgver/qt-creator-opensource-src-$pkgver.tar.gz"
ldpath="/usr/lib/qtcreator"
builddir="$srcdir/$pkgname-opensource-src-$pkgver"

build() {
	export LLVM_INSTALL_DIR=/usr
	qmake -r "QMAKE_CFLAGS += $CFLAGS" "QMAKE_CXXFLAGS += $CXXFLAGS" "QMAKE_LFLAGS += $LDFLAGS" "QMAKE_LIBS += -lexecinfo"
	make
}

package() {
	make install INSTALL_ROOT="$pkgdir"/usr
}

sha512sums="137fda4882bce31b2844c6a4c1d54f5628f0812611266b2d7f74d6c155bec97809f50dca4a253c5d9686e7d0838ee568d054e5102333c63c30ca430f50370013  qt-creator-opensource-src-4.12.4.tar.gz"
