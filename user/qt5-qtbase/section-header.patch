From ba6e0e4aac4d06782325c7032c8ea475f2d3eab0 Mon Sep 17 00:00:00 2001
From: David Faure <david.faure@kdab.com>
Date: Sat, 13 Apr 2019 19:37:37 +0200
Subject: [PATCH 1/1] QHeaderView: fix assert when restoring section sizes over
 less columns
MIME-Version: 1.0
Content-Type: text/plain; charset=utf8
Content-Transfer-Encoding: 8bit

If columns are removed and we get notified via layoutChanged, the code
tries to restore old section sizes, and went out of bounds, leading to
an assert in QVector. Simply add an if() to skip restoring out-of-bounds columns.

This comes from https://bugs.kde.org/show_bug.cgi?id=395181,
which translates into the unittest that is part of this commit.

Change-Id: Ide42176a758f87b21957c40508127d67f1d5a2d9
Reviewed-by: Christian Ehrlicher <ch.ehrlicher@gmx.de>
Reviewed-by: ThorbjÃ¸rn Lund Martsum <tmartsum@gmail.com>
---
 src/widgets/itemviews/qheaderview.cpp                   | 16 +++++++++-------
 .../widgets/itemviews/qheaderview/tst_qheaderview.cpp   | 17 +++++++++++++++++
 2 files changed, 26 insertions(+), 7 deletions(-)

diff --git a/src/widgets/itemviews/qheaderview.cpp b/src/widgets/itemviews/qheaderview.cpp
index 62abf567511..99309633a7b 100644
--- a/src/widgets/itemviews/qheaderview.cpp
+++ b/src/widgets/itemviews/qheaderview.cpp
@@ -2283,13 +2283,15 @@ void QHeaderViewPrivate::_q_sectionsChanged(const QList<QPersistentModelIndex> &
                                      : index.row());
         // the new visualIndices are already adjusted / reset by initializeSections()
         const int newVisualIndex = visualIndex(newLogicalIndex);
-        auto &newSection = sectionItems[newVisualIndex];
-        newSection = item.section;
-
-        if (newSection.isHidden) {
-            // otherwise setSectionHidden will return without doing anything
-            newSection.isHidden = false;
-            q->setSectionHidden(newLogicalIndex, true);
+        if (newVisualIndex < sectionItems.count()) {
+            auto &newSection = sectionItems[newVisualIndex];
+            newSection = item.section;
+
+            if (newSection.isHidden) {
+                // otherwise setSectionHidden will return without doing anything
+                newSection.isHidden = false;
+                q->setSectionHidden(newLogicalIndex, true);
+            }
         }
     }
 
diff --git a/tests/auto/widgets/itemviews/qheaderview/tst_qheaderview.cpp b/tests/auto/widgets/itemviews/qheaderview/tst_qheaderview.cpp
index eaf75e74945..1b3e1e1f34e 100644
--- a/tests/auto/widgets/itemviews/qheaderview/tst_qheaderview.cpp
+++ b/tests/auto/widgets/itemviews/qheaderview/tst_qheaderview.cpp
@@ -248,6 +248,7 @@ private slots:
     void sizeHintCrash();
     void testResetCachedSizeHint();
     void statusTips();
+    void testRemovingColumnsViaLayoutChanged();
 
 protected:
     void setupTestData(bool use_reset_model = false);
@@ -353,6 +354,7 @@ public:
 
     void cleanup()
     {
+        emit layoutAboutToBeChanged();
         cols = 3;
         rows = 3;
         emit layoutChanged();
@@ -3489,5 +3491,20 @@ void tst_QHeaderView::statusTips()
     QCOMPARE(headerView.statusTipText, QLatin1String("[0,1,0] -- Header"));
 }
 
+void tst_QHeaderView::testRemovingColumnsViaLayoutChanged()
+{
+    const int persistentSectionSize = 101;
+
+    QtTestModel model;
+    model.rows = model.cols = 5;
+    view->setModel(&model);
+    for (int i = 0; i < model.cols; ++i)
+        view->resizeSection(i, persistentSectionSize + i);
+    model.cleanup(); // down to 3 via layoutChanged (not columnsRemoved)
+    for (int j = 0; j < model.cols; ++j)
+        QCOMPARE(view->sectionSize(j), persistentSectionSize + j);
+    // The main point of this test is that the section-size restoring code didn't go out of bounds.
+}
+
 QTEST_MAIN(tst_QHeaderView)
 #include "tst_qheaderview.moc"
-- 
2.16.3

