# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qtbase
_pkgname=qtbase-everywhere-src
pkgver=5.12.9
pkgrel=0
pkgdesc="Cross-platform application and UI framework"
url="https://www.qt.io/"
arch="all"
license="LGPL-3.0-only WITH Qt-LGPL-exception-1.1 OR GPL-3.0-only WITH Qt-GPL-exception-1.0"
_sub="$pkgname-sqlite $pkgname-odbc $pkgname-postgresql
	$pkgname-tds $pkgname-x11"
depends=""
depends_dev="dbus-dev fontconfig-dev freetype-dev glib-dev libice-dev
	libpng-dev libsm-dev libx11-dev libxext-dev mesa-dev openssl-dev perl
	sqlite-dev zlib-dev $_sub"
makedepends="$depends_dev
	at-spi2-core-dev bison cups-dev eudev-dev flex freetds-dev freetype-dev
	gperf gtk+2.0-dev harfbuzz-dev hicolor-icon-theme icu-dev
	libexecinfo-dev libinput-dev libjpeg-turbo-dev libxkbcommon-dev
	libxi-dev libxrandr-dev libxrender-dev libxslt-dev libxv-dev mtdev-dev
	pcre2-dev postgresql-dev unixodbc-dev xcb-util-dev xcb-util-image-dev
	xcb-util-keysyms-dev xcb-util-wm-dev xcb-util-renderutil-dev cmd:which
	"
subpackages="$pkgname-dev $pkgname-doc $_sub"
source="https://download.qt.io/official_releases/qt/${pkgver%.*}/$pkgver/submodules/$_pkgname-$pkgver.tar.xz
	big-endian-scroll-wheel.patch
	forward-port-move.patch
	link-to-execinfo.patch
	qt-musl-iconv-no-bom.patch
	time64.patch
	CVE-2020-17507.patch
	section-header.patch
	"

# secfixes: qt
#   5.9.7-r0:
#     - CVE-2018-15518
#     - CVE-2018-19869
#     - CVE-2018-19870
#     - CVE-2018-19871
#     - CVE-2018-19873
#   5.12.6-r1:
#     - CVE-2020-0569
#     - CVE-2020-0570
#   5.12.9-r0:
#     - CVE-2015-9541
#     - CVE-2020-13962
#     - CVE-2020-17507

_qt5_prefix=/usr/lib/qt5
_qt5_datadir=/usr/share/qt5
builddir="$srcdir"/$_pkgname-$pkgver

prepare() {
	default_prepare
	sed -i -e "s|-O2|$CXXFLAGS|" \
		-e "/^QMAKE_RPATH/s| -Wl,-rpath,||g" \
		-e "/^QMAKE_LFLAGS\s/s|+=|+= $LDFLAGS|g" \
		mkspecs/common/linux.conf
}

build() {
	ARCH_OPTS=""
	[ ${CTARGET_ARCH} != "pmmx" ] || ARCH_OPTS="-no-sse2"

	./configure -confirm-license -opensource \
		-archdatadir "$_qt5_prefix" \
		-bindir "$_qt5_prefix"/bin \
		-datadir "$_qt5_datadir" \
		-dbus-linked \
		-docdir /usr/share/doc/qt5 \
		-examplesdir /usr/share/doc/qt5/examples \
		-glib \
		-icu \
		-importdir "$_qt5_prefix"/imports \
		-kms \
		-libexecdir "$_qt5_prefix"/libexec \
		-no-pch \
		-no-rpath \
		-nomake examples \
		-opengl \
		-openssl-linked \
		-optimized-qmake \
		-plugin-sql-odbc \
		-plugin-sql-psql \
		-plugin-sql-sqlite \
		-plugin-sql-tds \
		-plugindir "$_qt5_prefix"/plugins \
		-prefix /usr \
		-silent \
		-sysconfdir /etc/xdg \
		-system-freetype \
		-system-harfbuzz \
		-system-libjpeg \
		-system-libpng \
		-system-pcre \
		-system-sqlite \
		-system-xcb \
		-system-zlib \
		-translationdir "$_qt5_datadir"/translations \
		-no-reduce-relocations \
		-force-debug-info \
		$ARCH_OPTS
	make
}

check() {
	make check
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	mkdir -p "$pkgdir"/usr/bin/
	for i in "$pkgdir"/$_qt5_prefix/bin/*; do
		ln -s ../lib/qt5/bin/"$(basename $i)" \
			"$pkgdir"/usr/bin/"$(basename $i)"
	done
}

_mv_files() {
	for i in "$@"; do
		mkdir -p "$subpkgdir"/${i%/*}
		mv "$pkgdir"/$i "$subpkgdir"/${i%/*}
	done
}

dev() {
	cd "$pkgdir"  # Required for the 'find' command to work properly.
	_mv_files usr/lib/qt5/mkspecs \
		usr/lib/cmake \
		usr/lib/qt5/bin \
		usr/bin \
		$(find usr/ -name '*.prl')
	default_dev
}

sqlite() {
	pkgdesc="SQLite driver for Qt5's SQL classes"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlite*
}

odbc() {
	pkgdesc="ODBC driver for Qt5's SQL classes"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlodbc*
}

postgresql() {
	pkgdesc="PostgreSQL driver for Qt5's SQL classes"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlpsql*
}

mysql() {
	pkgdesc="MySQL driver for Qt5's SQL classes"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlmysql*
}

tds() {
	pkgdesc="TDS driver for Qt5's SQL classes"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqltds*
}

x11() {
	pkgdesc="Qt5 GUI-related libraries"
	depends="hicolor-icon-theme"
	_mv_files \
		usr/lib/libQt5EglFSDeviceIntegration.so.* \
		usr/lib/libQt5EglFsKmsSupport.so.* \
		usr/lib/libQt5Gui.so.* \
		usr/lib/libQt5OpenGL.so.* \
		usr/lib/libQt5PrintSupport.so.* \
		usr/lib/libQt5Widgets.so.* \
		usr/lib/libQt5XcbQpa.so.* \
		usr/lib/qt5/plugins/egldeviceintegrations \
		usr/lib/qt5/plugins/generic \
		usr/lib/qt5/plugins/image* \
		usr/lib/qt5/plugins/platform* \
		usr/lib/qt5/plugins/printsupport* \
		usr/lib/qt5/plugins/xcbglintegrations

	scanelf -Rn "$pkgdir"/usr/ | egrep '(libX|libQt5Gui|libGL)' && return 1
	return 0
}

sha512sums="40916f73e44dbcab2a3196063d491d5563ec3de583436dac25ecf219aea6e7eb55c46ce8b1c761980f90495b91c89bd5239bd081636054311fee6420750319b0  qtbase-everywhere-src-5.12.9.tar.xz
d00dc607b71a93132f756b952871df9197cfd6d78cc3617544bfa11d7f0eea21ce5dd0d1aeb69dd2702a5694a63d3802accc76499dbf414c01eb56421698cb0c  big-endian-scroll-wheel.patch
e8c542a7c203e7d449f06ac16784e13aefef55f6b8ef8619c49e4604e6e4f697baef027415d1b65f69e95770479c2ed0074dc0dfff9632317eccb30701ae1ed3  forward-port-move.patch
ee78a44e28ba5f728914bfc3d8d5b467896c7de11a02d54b0bce11e40a4338b1f776c1fcc30cbd436df4f548c1ab0b4fe801f01b162ddd5c0f892893e227acfd  link-to-execinfo.patch
e3982b2df2ab4ba53b7a1329a9eb928eb1fee813c61cf6ac03d3300a767ffb57f019ac0fd89f633cac2330549446ff3d43344871296bf362815e7ebffadefa6b  qt-musl-iconv-no-bom.patch
436f0bb7a89a88aa62c7b0398c4e91c325e78542e96f747c903f7e96dbf9d9b693d9688c722f2a74e287fb9ab31e861bd5ed8deb172ed28f56a1b8757663771c  time64.patch
9ebf15139025d76ff103a1ae77973136b2f883a38dc54febfa44f08060f41ee13016668c96a29c62dcc458125516ba8bdb899b1ab5604dc976b4f72e513bb682  CVE-2020-17507.patch
47b2973561965e3ef906f03480b3877ad0018f32d31fecb4c410abe22c68ccad7d232cfe68804b70111616e15b979fb26642225b984d8fdbfc6cf6899ad63a0d  section-header.patch"
