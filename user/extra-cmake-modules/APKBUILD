# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=extra-cmake-modules
pkgver=5.74.0
pkgrel=0
pkgdesc="CMake modules needed for KDE development"
url="https://www.kde.org/"
arch="noarch"
options="!dbg"
license="BSD-3-Clause"
depends=""
makedepends="cmake qt5-qtbase-dev qt5-qttools-dev qt5-qtdeclarative-dev
	qt5-qtquickcontrols py3-sphinx"
subpackages="$pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz
	posix.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE \
		ctest -E '(relative_or_absolute_|KDEFetchTranslations)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5d4f612758bda73d8327cc582bda3550807de90e1b729d24a59ac7e70db127da208a53590f48cda9775f2be6e22a7db47958e3f01577b4baf0b8ef86672579a1  extra-cmake-modules-5.74.0.tar.xz
a9e5d5e7ac8372099458ed18d2a6023fa0acf46955f51509880e7a467b4bd9e5df67c44c9ad032b1d70139efb73206390eaf7cd2baf63a82131e6e2b4acdbd71  posix.patch"
