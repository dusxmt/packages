# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=ksysguard
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE system monitor utility"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	ki18n-dev kiconthemes-dev kinit-dev kitemviews-dev knewstuff-dev
	knotifications-dev kwindowsystem-dev libksysguard-dev
	cmake extra-cmake-modules kdoctools-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/ksysguard-$pkgver.tar.xz
	ksysguard-5.6.5-rindex-header.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2cfb82caef5d074b945f0a7478b32af86d49e174d4d294dfe6a54acdb0b1d335e151828a4f239b52cf767ba04d9e93bf1632f2a37e2b1d611128543e0e0008a3  ksysguard-5.18.5.tar.xz
75e07b80c647bc6fdfb092ad3855813ef6bfe91b0ad03562dacfe4e680cfdee201364a2e452db162822045684635c1791ab2392c47d8e5c560c9e617a970cf39  ksysguard-5.6.5-rindex-header.patch"
