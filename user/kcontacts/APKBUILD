# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcontacts
pkgver=5.74.0
pkgrel=0
pkgdesc="Library for working with contact information"
url="https://www.kde.org"
arch="all"
license="LGPL-2.1-only"
depends="iso-codes"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev kcoreaddons-dev
	ki18n-dev kcodecs-dev iso-codes-dev doxygen graphviz qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcontacts-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# addresstest requires the library to already be installed.
	# picturetest requires X11 running.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(address|picture)test"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a4c78e0676a52abbee473310924280a7776c79bec3cf2273379f0c90e904ea0a31f5b5bd372472b29bfa5ec3a7ca5764af9e422bfc6a06fe3158287182815c71  kcontacts-5.74.0.tar.xz"
