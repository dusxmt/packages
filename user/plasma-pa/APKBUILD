# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-pa
pkgver=5.18.5
pkgrel=0
pkgdesc="PulseAudio integration for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kirigami2 libcanberra-pulse sound-theme-freedesktop"
makedepends="cmake extra-cmake-modules glib-dev kcoreaddons-dev kdeclarative-dev
	kdoctools-dev kglobalaccel-dev ki18n-dev knotifications-dev
	libcanberra-dev plasma-framework-dev pulseaudio-dev qt5-qtbase-dev
	qt5-qtdeclarative-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-pa-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fff3049d4879e4f6fe428985dd14f91fa6bac8fe2e7c696ee1424d610038f61fd7eda2621bfd2c2643529956f7986b59c9eead1ceb79e1cfb1397fa52b470376  plasma-pa-5.18.5.tar.xz"
