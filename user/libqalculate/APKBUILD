# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libqalculate
pkgver=3.6.0
pkgrel=0
pkgdesc="Library implementing a powerful, versatile desktop calculator"
url="https://qalculate.github.io/"
arch="all"
options="!check"  # missing potfiles
license="GPL-2.0+"
depends=""
depends_dev="gmp-dev mpfr-dev"
makedepends="$depends_dev curl-dev icu-dev intltool libxml2-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang qalc"
source="https://github.com/Qalculate/libqalculate/releases/download/v$pkgver/libqalculate-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

qalc() {
	pkgdesc="Powerful, versatile desktop calculator (terminal UI)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="afef26633d28f4c9b018a17056bf9645118b2e8698d4d5ba76a4a7df783503579381db448a5d557059fc7629d175385353d7eabeef3636ec0cad669fd68e5d0f  libqalculate-3.6.0.tar.gz"
