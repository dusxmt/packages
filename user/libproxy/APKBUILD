# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=libproxy
pkgver=0.4.15
pkgrel=3
pkgdesc="Library providing automatic proxy configuration management"
url="http://libproxy.github.io/libproxy/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="zlib-dev"
makedepends="cmake python3-dev $depends_dev"
subpackages="$pkgname-dev $pkgname-bin py3-$pkgname:py"
source="$pkgname-$pkgver.tar.gz::https://github.com/libproxy/libproxy/archive/$pkgver.tar.gz
	libproxy-0.4.7-unistd.patch
	fix-includes.patch
	CVE-2020-25219.patch
	CVE-2020-26154.patch
	"

# secfixes:
#   0.4.15-r3:
#     - CVE-2020-25219
#     - CVE-2020-26154

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIBEXEC_INSTALL_DIR=lib \
		-DMODULE_INSTALL_DIR=/usr/lib/libproxy/$pkgver/modules \
		-DWITH_PERL=OFF \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/cmake "$subpkgdir"/usr/share/
}

bin() {
	pkgdesc="Binary to test libproxy"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

py() {
	pkgdesc="Binding for libproxy and python"
	replaces="py-$pkgname"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python* "$subpkgdir"/usr/lib/
}

sha512sums="8f68bd56e44aeb3f553f4657bef82a5d14302780508dafa32454d6f724b724c884ceed6042f8df53a081d26ea0b05598cf35eab44823257c47c5ef8afb36442b  libproxy-0.4.15.tar.gz
9929c308195bc59c1b9a7ddaaf708fb831da83c5d86d7ce122cb9774c9b9b16aef3c17fb721356e33a865de1af27db493f29a99d292e1e258cd0135218cacd32  libproxy-0.4.7-unistd.patch
e35b4f806e5f60e9b184d64dceae62e6e343c367ee96d7e461388f2665fe2ab62170d41848c9da5322bb1719eff3bfaecb273e40a97ce940a5e88d29d03bd8d9  fix-includes.patch
908fbf49bec18764a8c2ab81ef5d5e6e1fc2423cf9a6608cc7d3a6d5ac44676e171646b0f95b39b7ade108afd62cc2ede8f7b57d6ba0d67025f30b18e5084292  CVE-2020-25219.patch
01c784a8016bb2a2bf5058b6af7fac29250542bfd4e0679a91fa223c821336d651f8f4a968763072edb86a78a743618c312a2daeb2963c8e5207109f2d26a18f  CVE-2020-26154.patch"
