# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kalzium
pkgver=20.08.1
pkgrel=0
pkgdesc="Periodic table of elements (PSE) with calculators"
url="https://www.kde.org/applications/education/kalzium/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev
	eigen-dev qt5-qtdeclarative-dev qt5-qtsvg-dev karchive-dev kconfig-dev
	kcoreaddons-dev kdoctools-dev ki18n-dev kdelibs4support-dev khtml-dev
	kparts-dev kplotting-dev solid-dev kunitconversion-dev
	kwidgetsaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalzium-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="60da587ba5001b718bd6106a384c26b5351e41d943ad6796def4b1e8e8c8aa8c077f58019e4a6e6796a90a3d594dfa61602cd149d82415d7cf7df0e6f453c5ee  kalzium-20.08.1.tar.xz"
