# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=keepassxc
pkgver=2.6.0
pkgrel=0
pkgdesc="A community revival of the KeePassX password manager"
url="https://keepassxc.org"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND MIT AND BSD-4-Clause AND ISC AND (LGPL-2.1-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only) AND CC0-1.0 AND Public-Domain AND LGPL-2.1+ AND LGPL-3.0+"
depends="hicolor-icon-theme"
makedepends="argon2-dev cmake libgcrypt-dev libqrencode-dev libsodium-dev
	libxi-dev libxtst-dev qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev
	qt5-qtx11extras-dev xz zlib-dev asciidoctor"
subpackages="$pkgname-doc"
source="https://github.com/keepassxreboot/$pkgname/releases/download/$pkgver/$pkgname-$pkgver-src.tar.xz"

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DWITH_XC_BROWSER=bool:ON \
		-DWITH_XC_FDOSECRETS=bool:ON \
		-DWITH_XC_SSHAGENT=bool:ON \
		-Bbuild
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="c1ddf81f965f5521db8b8acb2fe3950c7b185ca459c440c9118f3d6350eeff6f15bb488f9e3bd034bdf80c515f0753c041e3af083d62e179cd16df7bb8326e17  keepassxc-2.6.0-src.tar.xz"
