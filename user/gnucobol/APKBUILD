# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=gnucobol
pkgver=3.0_rc1
_pkgver=3.0-rc1
pkgrel=0
pkgdesc="Free COBOL compiler"
url="https://sourceforge.net/projects/open-cobol/"
arch="all"
license="GPL-3.0+ AND LGPL-3.0+ AND GFDL-1.3"
# Since it compiles to C as an intermediate, the headers / libraries must
# always be present and thus there is no -dev subpackage.
depends="db-dev gmp-dev ncurses-dev"
makedepends="autoconf automake help2man libtool texinfo"
subpackages="$pkgname-doc $pkgname-lang"
source="https://downloads.sourceforge.net/project/open-cobol/gnu-cobol/3.0/$pkgname-$_pkgver.tar.xz
	https://dev.sick.bike/dist/newcob.val.Z
	keep-debug-flags.patch
	big-endian.patch
	tests-are-fatal.patch
	dash-compat.patch
	"
builddir="$srcdir/$pkgname-$_pkgver"

prepare() {
	default_prepare
	autoreconf -vif
	# For "make test"
	cp "$srcdir/newcob.val.Z" "$builddir/tests/cobol85"
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-debug
	make
}

check() {
	make check
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="da669f7bef0c6664723dc88c20e28236ea31d5c933d9129b0132e929eb66f723995d3428b056164cf105bc22b38b8bc5fbf9d0d5b76df4ce6c08045837930fa0  gnucobol-3.0-rc1.tar.xz
eb784866d1a335834074fbe103b5c5f5da810d847ae14a64316ecbb2c0f95ef751d2f2ec609b8fa21ef0b775817dc61115a574608ec89750d779ae0afbb95379  newcob.val.Z
f45611c1298680b0c763094af662b3d1925d32379d33faff77862d7eaa3bf6225f68d5160a39a0822f54b570c923a53bc72dec54cff4f04f1162515834a40be2  keep-debug-flags.patch
f8f8bb395e3f2e0e993df5133d1ed459dc7f03f148d76ddbbe4820f7674300c8c9a8d024785124444ca71a18a0d917e816ab852e0b5e9ebbfa5ed35f12a80b0f  big-endian.patch
ca5ba1e19e5ff8740008b45ffcea4c1e8e34fc62246f3e635424ae1ec374c5cd9d7cc94d371690c021ccf82e293db7235143ebcaadb20ae5e4fc258749ff56c4  tests-are-fatal.patch
376af9b264b679388722b4d489a834cfb24b47fbdb6cd9bd9c239894741ffc265c2f83d770b0a820f48b3668b3596544183b48bee319186e4ed5c9c60bad4e77  dash-compat.patch"
