# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=mpv
pkgver=0.32.0
pkgrel=0
pkgdesc="An improved fork of mplayer"
url="https://mpv.io"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+ AND GPL-2.0+ AND ISC AND BSD-2-Clause AND MIT AND BSD-3-Clause"
depends=""
makedepends="python3
	zlib-dev libarchive-dev py3-docutils uchardet-dev ncurses-dev
	
	alsa-lib-dev pulseaudio-dev

	libx11-dev libxext-dev libxinerama-dev libxrandr-dev libxscrnsaver-dev
	mesa-dev libva-dev lcms2-dev libvdpau-dev

	ffmpeg-dev libbluray-dev v4l-utils-dev libass-dev libdvdread-dev
	libdvdnav-dev libcdio-dev libcdio-paranoia-dev rubberband-dev
	"
subpackages="$pkgname-doc"
source="mpv-$pkgver.tar.gz::https://github.com/mpv-player/mpv/archive/v$pkgver.tar.gz"

build() {
	python3 ./bootstrap.py
	python3 ./waf configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--destdir="$pkgdir" \
		--disable-lua \
		--disable-javascript \
		--disable-wayland \
		--disable-gl-wayland \
		--enable-dvdnav \
		--enable-cdda
	python3 ./waf build
}

package() {
	python3 ./waf install --destdir="$pkgdir"
}

sha512sums="f6426c0254ec0bf2f120e2196904f1e15fe17032b06764abca1d5e074f0cabb452eaf1cd09f8fd9b25b591accee7b881bfc3b06c19d5c98980305c4712486bd6  mpv-0.32.0.tar.gz"
