# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ark
pkgver=20.08.1
pkgrel=0
pkgdesc="Graphical file compression/decompression utility with support for multiple formats"
url="https://utils.kde.org/projects/ark/"
arch="all"
options="!check"  # requires other formats not packaged and manual user input
license="GPL-2.0-only"
depends="lzop shared-mime-info unzip zip"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kcrash-dev kdbusaddons-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kitemmodels-dev kio-dev kservice-dev kparts-dev kpty-dev libarchive-dev
	kwidgetsaddons-dev libzip-dev xz-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/ark-$pkgver.tar.xz"

# secfixes:
#   20.08.1-r0:
#    - CVE-2020-16116
#    - CVE-2020-24654

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1fae786d17a6e576e64b5b72e7d6886900a2fee3eedad41db174382dc70cb858c5c192c20896e5c2b6ec3c07f07d155fa5f52654496876808650a279b39eaa86  ark-20.08.1.tar.xz"
