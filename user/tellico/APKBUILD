# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tellico
pkgver=3.3.1
pkgrel=0
pkgdesc="Collection manager"
url="https://tellico-project.org/"
arch="all"
license="GPL-2.0-only OR GPL-3.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxml2-dev libxslt-dev
	karchive-dev kcodecs-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kdoctools-dev kguiaddons-dev khtml-dev ki18n-dev
	kiconthemes-dev kio-dev kitemmodels-dev kjobwidgets-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev solid-dev

	kfilemetadata-dev knewstuff-dev libcdio-dev libksane-dev ncurses-dev
	poppler-dev poppler-qt5-dev taglib-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="http://tellico-project.org/files/tellico-$pkgver.tar.xz
	btparse-strcasecmp.patch
	modern-taglib.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -std=gnu99" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# imagejob, cite, csv, pdf: needs running X11
	# htmlexporter: needs plasma desktop
	# filelisting: needs dbus
	# tellicoread: needs network
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(cite|csv|filelisting|imagejob|htmlexporter|pdf|tellicoread|comparison)test'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="12d49aba8c99cd16e426ac0144fa7343c5b1348a8c9a0892f30ff274f0e98f9ad7819c03c4ecdd0dc2f826377468ebdab1996828e992fcd293c7ffe4f844ab74  tellico-3.3.1.tar.xz
4627e717d67340de6d88f7a21604a66ba236c651a0ae38d9d3569b76ad58c79f046cfd5686dd688de86d6acafc17ba3959902babdc7f00ab8e9d65717c4fab4a  btparse-strcasecmp.patch
1d2b7825249167868b4127e424b3763551d9c361c500bb251b2e22e6e0b938e3015454092ec9e4724b2fd7a3bc1b059f9ead52cf75b7c85ca3a66d0fc3fe57e5  modern-taglib.patch"
