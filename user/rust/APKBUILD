# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Shiz <hi@shiz.me>
# Contributor: Jeizsm <jeizsm@gmail.com>
# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.38.0
_bootver=1.37.0
_llvmver=8
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	libgit2-dev
	libssh2-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	zlib-dev
	cargo-bootstrap~$_bootver
	$pkgname-bootstrap~$_bootver
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	miri
	rls
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Don-t-pass-CFLAGS-to-the-C-compiler.patch
	0002-Fix-LLVM-build.patch
	0003-Allow-rustdoc-to-work-when-cross-compiling-on-musl.patch
	0004-Require-static-native-libraries-when-linking-static-.patch
	0005-Remove-nostdlib-and-musl_root-from-musl-targets.patch
	0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0007-Fix-C-aggregate-passing-ABI-on-powerpc.patch
	0008-Fix-zero-sized-aggregate-ABI-on-powerpc.patch
	0009-compiletest-Match-suffixed-environments.patch
	0010-test-c-variadic-Fix-patterns-on-powerpc64.patch
	0011-Use-rustc-workspace-hack-for-rustbook.patch
	0012-test-failed-doctest-output-Fix-normalization.patch
	0013-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0014-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0015-Ignore-broken-and-non-applicable-tests.patch
	0016-Link-stage-2-tools-dynamically-to-libstd.patch
	0017-Move-debugger-scripts-to-usr-share-rust.patch
	0018-Add-foxkit-target-specs.patch
	0030-libc-linkage.patch
	0031-typenum-pmmx.patch
	0032-libgit2-sys-abi.patch
	0040-rls-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		[build]
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rls", "rustfmt", "src" ]
		[install]
		prefix = "/usr"
		[rust]
		codegen-units = 1
		codegen-units-std = 1
		debug-assertions = false
		debuginfo-level = 2
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		rpath = false
		verbose-tests = true
		dist-src = false
		jemalloc = false
		llvm-libunwind = false
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
	EOF

	LIBGIT2_SYS_USE_PKG_CONFIG=1 \
	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LIBGIT2_SYS_USE_PKG_CONFIG=1 \
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast \
		src/test/codegen \
		src/test/codegen-units \
		src/test/compile-fail \
		src/test/incremental \
		src/test/mir-opt \
		src/test/pretty \
		src/test/run-fail \
		src/test/run-make \
		src/test/run-make-fulldeps \
		src/test/rustdoc \
		src/test/rustdoc-js \
		src/test/rustdoc-js-std \
		src/test/rustdoc-ui \
		src/test/ui \
		src/test/ui-fulldeps
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.gz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.gz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/*.so \
	   "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

miri() {
	pkgdesc="An interpreter for Rust's mid-level intermediate representation"
	license="Apache-2.0 OR MIT"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/miri "$subpkgdir"/usr/bin
}

rls() {
	pkgdesc="The Rust language server"
	license="Apache-2.0 OR MIT"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rls "$subpkgdir"/usr/bin
}

rustfmt() {
	pkgdesc="Format Rust code"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="b756d29a7a222bc7b5c7f42ff397346ab840f78e559f93e6e36b65e76eea525cf429899fe4de9fb8966623a2225b552feef9fa831bee50f9e25c976fa2af8c0a  rustc-1.38.0-src.tar.xz
79ca3a7dcb2e448ab5798f1cfc62bba833c947dad684521c3c77b233ca3ce27301e706848f1b05c3405ebc843684f47a7809d2d9782b4f11be8c9941e1153027  0001-Don-t-pass-CFLAGS-to-the-C-compiler.patch
61a7072b554f8e90e65f1b1e9975f7359b8523026129003a51cb216b54d8fe2ffaa6ef6ee5aeb4defcc28fb5b2d874314c61988875bc86edfdaf706efab67cd9  0002-Fix-LLVM-build.patch
30404faeb82fa8236a481c91ba29c6ac6d4276140702164af3ec91eaaa4d13c547f2fce245ac0220a5a64ccc7c79818c2e5d70900f63fa17356e906a5b309915  0003-Allow-rustdoc-to-work-when-cross-compiling-on-musl.patch
7b1a77764d67c48a2c9054731dff0f245b70c249ff53cdaa0d4c26f6d01ae9522302bc487b889d22c9b82a926cbf08d938a4886e5197ea814d2b0a0f0066cd6e  0004-Require-static-native-libraries-when-linking-static-.patch
9a9d413cf866c44e1eadf51c736d376e87c83cdbee9509526666b01e73030e4e10ced0aca1b8575955f7dd90eb4b246e929ab36a624e212c8ff0ecf726914948  0005-Remove-nostdlib-and-musl_root-from-musl-targets.patch
4183bc73eaa4195d6fbfa79caf4c7bdc5ce9f29eb9e023b11252bd57f3e5daa4ea19753f7bad7db2f7872788e2dfe821aa50c7b1d63cbe42cd024742062f27b5  0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
177d54428bb200b6ab08c3ac1abd763c794494b80f25fa81732f1b355ecc0d3cb3ebb187fbc919aea2844577991fb66c308da27453ab1a21529dd6f70f2d2f90  0007-Fix-C-aggregate-passing-ABI-on-powerpc.patch
ad2b8d030576538a59498bbc529da7e4839b3d85d37981c83565ecd8605c0a797d1fc355a01ea05ff617ed6b6bd4c9f344420e9e8bf10374318d1de6df712a94  0008-Fix-zero-sized-aggregate-ABI-on-powerpc.patch
aefc44d8610436be9040329cf13fbe5c36f542b2124b533b6797fe239dcf706d004c73e319226ac19fb790c0114cdd3a49e153f66a154804dc84064cbcb32f70  0009-compiletest-Match-suffixed-environments.patch
489b97af8624a434b8267a52c88f1ab2a9f75c668148feb5a6570aaf9696b92cc49946b628089a9e56f22a7e35e6c9b9d1844505a024da14520ae420001c4734  0010-test-c-variadic-Fix-patterns-on-powerpc64.patch
d4711810dff2c59884efb2a141b5bbbdd43041d94b878b69b7fe9667b20e207851403ae1d5c1e100c4f17436bd1b82500207e8a45be5f7dc49449faae7b8d539  0011-Use-rustc-workspace-hack-for-rustbook.patch
f12a6eda193bb8d1bdfae568ade26de519e6d80ffa149ef31f64d052e44eeff3ce46220fe594118d4e4cf277eda19949636472b300226fb1e38ff70a214cbdea  0012-test-failed-doctest-output-Fix-normalization.patch
ae3fb54e0ec3a504cd58ad27159f00d2269266efd8723351df84e1c38d8345fa5ab85a0964b69cd6f0ccca1aa01a92c61f284a97fee7c0a299c1808251a48289  0013-test-use-extern-for-plugins-Don-t-assume-multilib.patch
3ed7fda61e943a110f9c5caa2b5015704f49c18139c86ec3d187faf9191703e5003c16c462e455b2bf889e0c0f7f137de52d8cfa283e78b174089297a89f9a1f  0014-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
4605c006c6b389672227b836b43e3cecdf808977d3502d0dd335f996ca51aaf5f0993ab23027f523e8b81baeb2ad9aba2775137e445698cf5526f41f0a4689d3  0015-Ignore-broken-and-non-applicable-tests.patch
5be32ec3b3dcbdf8061c57c68a6496b16e049cbcdcc5e17f75a9f76e41d5c309f4c8655ec8af3e2870c3426ded69b64878baad8e565222ea58fdf76b460f32c9  0016-Link-stage-2-tools-dynamically-to-libstd.patch
e8415a50fb981d8bff795ad1c869a791250401e8925f3a89e4b67badcb23938e9f39917bf0bcc298c29f13dc98e48ae1131f79af3cbdb1af3733b7f03ad579af  0017-Move-debugger-scripts-to-usr-share-rust.patch
3fc66d68c316300eafdbdf622aadb8afdbe9b4f8b110db40cac327692ed869b34a623448c3e4409a5bf2c53479a05359ecaa87c8bb65c3939c71346542aeafcf  0018-Add-foxkit-target-specs.patch
453dd39ae08b8fa633016d9a69a675b26edd86c2a881a3f30562f29e5098b86b4ad64804f453c0f9150696315f084555eb6d0c7aad3e72381f8844c7fc27b5ef  0030-libc-linkage.patch
0cb12e9165d198c1e04b258454dbaf5459e192ad24d64c9fa132ebe0f1bcd5da3550eae8dfdaa792fa809b505af62964ecf0219dc4373420ee8ad3e111539a09  0031-typenum-pmmx.patch
ccf6b0f91e39d247c296f5a4330d5dd36385cb58dea6987e8b0d31cc8aa6d6612e6c86a4b2576bde4be026048058dccb71fb87e68e7f72a7e7a4360ce4dce7f5  0032-libgit2-sys-abi.patch
ab35bacf45ef5e46be110a8d27867fd4d5deb23cd5cbe8dc7f1da2177469945f9254f2a7915ee4fc430468a4421623429f0a01eb9eba14e047384efe3d3ec152  0040-rls-atomics.patch"
