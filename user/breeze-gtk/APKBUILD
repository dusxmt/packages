# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=breeze-gtk
pkgver=5.18.5
pkgrel=0
pkgdesc="GTK+ style that matches KDE Breeze"
url="https://www.kde.org/"
arch="noarch"
license="LGPL-2.1-only"
depends="breeze gtk+2.0"
makedepends="breeze-dev cmake extra-cmake-modules py3-pycairo qt5-qtbase-dev sassc"
# XXX should be installed if breeze and gtk+3.0 are installed, as well
install_if="breeze~$pkgver gtk+2.0"
source="https://download.kde.org/stable/plasma/$pkgver/breeze-gtk-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3dea9af26cd18dc0388c76c070c5a375054b6a03ffa97e298341924fd909e2dcc49a7c7b431b36a158c1012ce6f9d48f98ef84fc6cda0965d9a24ab18c7c0951  breeze-gtk-5.18.5.tar.xz"
