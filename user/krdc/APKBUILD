# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krdc
pkgver=20.08.1
pkgrel=0
pkgdesc="View and control remote desktops (RDP and VNC)"
url="https://www.kde.org/applications/internet/krdc/"
arch="all"
license="GPL-2.0-only"
depends=""
depends_dev="qt5-qtbase-dev kwallet-dev kconfig-dev kcoreaddons-dev"
makedepends="cmake extra-cmake-modules kcmutils-dev kdnssd-dev kiconthemes-dev
	knotifications-dev kbookmarks-dev kdoctools-dev kxmlgui-dev ki18n-dev
	kcompletion-dev kwallet-dev kwidgetsaddons-dev knotifyconfig-dev
	libvncserver-dev $depends_dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5569d0a72ee99b5ef82d01fb746bffd32426d91b49901bb5b2ecfbb57b713ebed7fbf3d2211d4671486ea7704d26d29229aaa5a7b6e91cf7c204e08a0518d21c  krdc-20.08.1.tar.xz"
