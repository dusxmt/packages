# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgeography
pkgver=20.08.1
pkgrel=0
pkgdesc="Geography learning tool and trainer"
url="https://www.kde.org/applications/education/kgeography/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kxmlgui-dev kcrash-dev
	kwidgetsaddons-dev kcoreaddons-dev kconfigwidgets-dev ki18n-dev
	kitemviews-dev kiconthemes-dev kdoctools-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgeography-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a1de3e9efc855fa39c1247c30b6e1033d9cf3f2e9bc921560f848c4a73fe7882a3e4d8eea7e9fc4e5352f135d28fe44313fd45de3da0fe4cb2aec73a8878cc5d  kgeography-20.08.1.tar.xz"
