# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=glfw
pkgver=3.3.2
pkgrel=0
pkgdesc="OpenGL desktop development library"
url="https://www.glfw.org/"
arch="all"
options="!check"  # Tests require X11, GL, and manual intervention.
license="Zlib"
depends=""
depends_dev="mesa-dev"
makedepends="$depends_dev cmake doxygen libx11-dev libxrandr-dev libxcursor-dev
	libxi-dev libxinerama-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/glfw/glfw/releases/download/$pkgver/glfw-$pkgver.zip"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_SHARED_LIBS=ON \
		-DGLFW_BUILD_TESTS=OFF \
		-DGLFW_BUILD_EXAMPLES=OFF \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
	install -D -d "$builddir"/docs/html "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="3084f405f6ecfa182df0841ffb44248b716fc03282afd8e6dc9a1361897fe62a1ce049b95f662d79aaae45aa52208bb552b8c649284f7f6d9c12623ac3728e60  glfw-3.3.2.zip"
