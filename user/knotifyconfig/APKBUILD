# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knotifyconfig
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for configuring notifications"
url="https://www.kde.org/"
arch="all"
options="!check"  # No test suite, despite mentioning testing deps...
license="LGPL-2.0-only"
depends=""
depends_dev="qt5-qtbase-dev"
checkdepends="kconfigwidgets-dev knotifications-dev kwidgetsaddons-dev
	kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev kcompletion-dev kconfig-dev ki18n-dev kio-dev
	knotifications-dev phonon-dev qt5-qtspeech-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifyconfig-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="73957b4977ebccc47c53fa65c6352ff333af7a170a55a6021e5426df3715c167a43170fc6d82ee7a452246560cc57a976341a013c9ed7afc243b74a18c755ffe  knotifyconfig-5.74.0.tar.xz"
